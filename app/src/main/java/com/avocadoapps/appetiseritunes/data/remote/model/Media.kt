package com.avocadoapps.appetiseritunes.data.remote.model

import android.os.Parcelable
import com.google.gson.annotations.Expose
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Media(
    @Expose
    var wrapperType: String? = null,
    @Expose
    var kind: String? = null,
    @Expose
    var artistId: Double = 0.toDouble(),
    @Expose
    var collectionId: Double = 0.toDouble(),
    @Expose
    var trackId: Double = 0.toDouble(),
    @Expose
    var artistName: String? = null,
    @Expose
    var collectionName: String? = null,
    @Expose
    var trackName: String? = null,
    @Expose
    var collectionCensoredName: String? = null,
    @Expose
    var trackCensoredName: String? = null,
    @Expose
    var artistViewUrl: String? = null,
    @Expose
    var collectionViewUrl: String? = null,
    @Expose
    var trackViewUrl: String? = null,
    @Expose
    var previewUrl: String? = null,
    @Expose
    var artworkUrl30: String? = null,
    @Expose
    var artworkUrl60: String? = null,
    @Expose
    var artworkUrl100: String? = null,
    @Expose
    var collectionPrice: Float = 0.toFloat(),
    @Expose
    var trackPrice: Float = 0.toFloat(),
    @Expose
    var releaseDate: String? = null,
    @Expose
    var collectionExplicitness: String? = null,
    @Expose
    var trackExplicitness: String? = null,
    @Expose
    var discCount: Int = 0,
    @Expose
    var discNumber: Int = 0,
    @Expose
    var trackCount: Int = 0,
    @Expose
    var trackNumber: Int = 0,
    @Expose
    var trackTimeMillis: Long = 0.toLong(),
    @Expose
    var country: String? = null,
    @Expose
    var currency: String? = null,
    @Expose
    var primaryGenreName: String? = null,
    @Expose
    var contentAdvisoryRating: String? = null,
    @Expose
    var shortDescription: String? = null,
    @Expose
    var longDescription: String? = null,
    @Expose
    var description: String? = null,
    @Expose
    var isStreamable: Boolean = false
): Parcelable