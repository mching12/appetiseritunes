package com.avocadoapps.appetiseritunes.data.remote.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.util.ArrayList

data class MediaResult(
    @Expose
    var resultCount: Int = 0,
    @Expose @SerializedName("results")
    var mediaResult: ArrayList<Media>? = null
)