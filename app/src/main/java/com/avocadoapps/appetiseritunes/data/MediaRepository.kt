package com.avocadoapps.appetiseritunes.data

import android.annotation.SuppressLint
import android.content.Context
import androidx.lifecycle.MutableLiveData
import com.avocadoapps.appetiseritunes.R
import com.avocadoapps.appetiseritunes.data.helper.Util
import com.avocadoapps.appetiseritunes.data.local.MediaDao
import com.avocadoapps.appetiseritunes.data.local.MediaData
import com.avocadoapps.appetiseritunes.data.remote.ITunesApiService
import com.avocadoapps.appetiseritunes.data.remote.model.Media
import com.avocadoapps.appetiseritunes.data.remote.model.MediaListState
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class MediaRepository (
    private val context: Context,
    private val iTunesApiService: ITunesApiService,
    private val mediaDao: MediaDao
) {

    @SuppressLint("CheckResult")
    fun fetchMedia(state: MutableLiveData<MediaListState>, query: String = "star",
                   country: String = "au", media: String = "all") {
        if(Util.isInternetAvailable(context)) {
            iTunesApiService
                .searchMedia(query, country, media)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    if(it.isSuccessful && it.body() != null) {
                        val data = it.body()!!.mediaResult!!
                        saveMedia(data)
                        state.value = MediaListState.Success(data)
                    } else handleLocalFetch(state)
                }
        } else handleLocalFetch(state)
    }

    private fun saveMedia(mediaList: ArrayList<Media>) {
        mediaDao.refreshfreshData(Util.retrofitToRoom(mediaList))
    }

    private fun getMedia(): ArrayList<Media> {
        return Util.roomToRetrofit(mediaDao.fetchMedialist() as ArrayList<MediaData>)
    }

    private fun handleLocalFetch(state: MutableLiveData<MediaListState>) {
        if(getMedia().isEmpty())
            state.value = MediaListState.Error(context.getString(R.string.error_fetch_failed))
        else state.value = MediaListState.Success(getMedia())
    }
}