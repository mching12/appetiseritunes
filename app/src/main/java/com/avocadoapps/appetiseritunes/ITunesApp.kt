package com.avocadoapps.appetiseritunes

import android.app.Application
import com.avocadoapps.appetiseritunes.di.databaseModule
import com.avocadoapps.appetiseritunes.di.networkModule
import com.avocadoapps.appetiseritunes.di.repositoryModule
import com.avocadoapps.appetiseritunes.di.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class ITunesApp: Application() {

    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidLogger()
            androidContext(this@ITunesApp)
            modules(listOf(repositoryModule, networkModule,
                viewModelModule, databaseModule))
        }
    }
}