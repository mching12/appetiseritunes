package com.avocadoapps.appetiseritunes.di

import androidx.room.Room
import com.avocadoapps.appetiseritunes.data.MediaRepository
import com.avocadoapps.appetiseritunes.data.local.MediaDatabase
import com.avocadoapps.appetiseritunes.data.local.MediaDatabase.Companion.DATABASE_NAME
import com.avocadoapps.appetiseritunes.data.remote.APIServiceGenerator
import com.avocadoapps.appetiseritunes.data.remote.ITunesApiService
import com.avocadoapps.appetiseritunes.viewmodel.MediaListViewModel
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

private val iTunesApiService = APIServiceGenerator()
    .createService(ITunesApiService::class.java)

val networkModule = module {
    single { iTunesApiService }
}

val viewModelModule = module {
    viewModel { MediaListViewModel(get()) }
}

val databaseModule = module {
    single {
        Room.databaseBuilder(get(), MediaDatabase::class.java, DATABASE_NAME)
            .allowMainThreadQueries()
            .build()
    }
    single { get<MediaDatabase>().mediaDao() }
}

val repositoryModule = module {
    single { MediaRepository(androidContext(), get(), get()) }
}
