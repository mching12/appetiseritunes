package com.avocadoapps.appetiseritunes.view

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import com.avocadoapps.appetiseritunes.R
import com.avocadoapps.appetiseritunes.data.remote.model.Media
import com.avocadoapps.appetiseritunes.view.fragment.MediaDetailFragment
import com.avocadoapps.appetiseritunes.viewmodel.MediaListViewModel
import kotlinx.android.synthetic.main.activity_item_detail.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import android.net.Uri


/**
 * An activity representing a single Item detail screen. This
 * activity is only used on narrow width devices. On tablet-size devices,
 * item details are presented side-by-side with a list of items
 * in a [MediaListActivity].
 */
class MediaDetailActivity : BaseActivity() {

    private val viewModel: MediaListViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_item_detail)

        initUi(savedInstanceState)
        initViewModel()
    }

    private fun initUi(savedInstanceState: Bundle?) {
        setSupportActionBar(toolbar)
        fab.setOnClickListener { view ->
            if(viewModel.getTrackUrl() == null)
                showSnackbar(view, getString(R.string.error_no_track_link))
            else launchWebLink(viewModel.getTrackUrl()!!)
        }

        // Show the Up button in the action bar.
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        // savedInstanceState is non-null when there is fragment state
        // saved from previous configurations of this activity
        // (e.g. when rotating the screen from portrait to landscape).
        // In this case, the fragment will automatically be re-added
        // to its container so we don't need to manually add it.
        // For more information, see the Fragments API guide at:
        //
        // http://developer.android.com/guide/components/fragments.html
        //
        if (savedInstanceState == null) {
            // Create the detail fragment and add it to the activity
            // using a fragment transaction.
            supportFragmentManager.beginTransaction()
                .add(R.id.item_detail_container, MediaDetailFragment.newInstance())
                .commit()
        }
    }

    private fun initViewModel() {
        intent.getParcelableExtra<Media>(EXTRA_MEDIA)?.let {
            viewModel.setFocusedMedia(it)
        }
    }

    private fun launchWebLink(url: String) {
        val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
        startActivity(browserIntent)
    }

    override fun onOptionsItemSelected(item: MenuItem) =
        when (item.itemId) {
            android.R.id.home -> {
                // This ID represents the Home or Up button. In the case of this
                // activity, the Up button is shown. For
                // more details, see the Navigation pattern on Android Design:
                //
                // http://developer.android.com/design/patterns/navigation.html#up-vs-back

                navigateUpTo(Intent(this, MediaListActivity::class.java))
                true
            }
            else -> super.onOptionsItemSelected(item)
        }

    companion object {

        private const val EXTRA_MEDIA = "extra_media"

        fun launch(activity: Activity, media: Media) {
            Intent(activity, MediaDetailActivity::class.java).apply {
                putExtra(EXTRA_MEDIA, media)
                activity.startActivity(this)
            }
        }
    }
}
