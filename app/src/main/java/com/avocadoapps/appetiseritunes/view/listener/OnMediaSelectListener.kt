package com.avocadoapps.appetiseritunes.view.listener

import com.avocadoapps.appetiseritunes.data.remote.model.Media

interface OnMediaSelectListener {
    fun onMediaSelected(media: Media)
}