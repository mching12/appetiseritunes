package com.avocadoapps.appetiseritunes.view.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.avocadoapps.appetiseritunes.R
import com.avocadoapps.appetiseritunes.data.remote.model.Media
import com.avocadoapps.appetiseritunes.view.listener.OnMediaSelectListener
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions
import kotlinx.android.synthetic.main.list_item_media.view.*

class MediaAdapter constructor(
    private val mediaSelectListener: OnMediaSelectListener,
    private val mediaList: ArrayList<Media>
): RecyclerView.Adapter<MediaAdapter.MediaViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MediaViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.list_item_media, parent, false)
        return MediaViewHolder(view)
    }

    override fun onBindViewHolder(holder: MediaViewHolder, position: Int) {
        holder.bind(mediaList[position], mediaSelectListener)
    }

    override fun getItemCount() = mediaList.size

    class MediaViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        @SuppressLint("SetTextI18n")
        fun bind(media: Media, mediaSelectListener: OnMediaSelectListener) {
            loadImage(itemView.ivMediaArt, media)
            itemView.tvMediaName.text = media.trackName
            itemView.tvMediaGenre.text = media.primaryGenreName
            itemView.tvMediaPrice.text = "${media.currency}$ ${media.trackPrice}"
            itemView.parentView.setOnClickListener {
                mediaSelectListener.onMediaSelected(media)
            }
        }

        private fun loadImage(image: ImageView, media: Media) {
            if (!media.artworkUrl60.isNullOrBlank()) {
                Glide.with(image.context)
                    .load(media.artworkUrl60)
                    .apply(RequestOptions.placeholderOf(R.drawable.ic_placeholder))
                    .transition(DrawableTransitionOptions.withCrossFade())
                    .into(image)
            }
        }
    }
}