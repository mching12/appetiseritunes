package com.avocadoapps.appetiseritunes.view.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import com.avocadoapps.appetiseritunes.R
import com.avocadoapps.appetiseritunes.data.helper.Util
import com.avocadoapps.appetiseritunes.data.remote.model.Media
import com.avocadoapps.appetiseritunes.viewmodel.MediaListViewModel
import kotlinx.android.synthetic.main.activity_item_detail.*
import kotlinx.android.synthetic.main.item_detail.*
import org.koin.androidx.viewmodel.ext.android.sharedViewModel

/**
 * A fragment representing a single Item detail screen.
 * This fragment is either contained in a [MediaListActivity]
 * in two-pane mode (on tablets) or a [MediaDetailActivity]
 * on handsets.
 */
class MediaDetailFragment : Fragment() {

    /**
     * The dummy content this fragment is presenting.
     */
    private val viewModel: MediaListViewModel by sharedViewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel.getFocusedMedia().observe(this, Observer {
            activity?.toolbar_layout?.title = it.trackName
            showMediaDetails(it)
        })
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.item_detail, container, false)
    }

    private fun showMediaDetails(media: Media) {
        tvDescription.text = media.longDescription ?: media.shortDescription ?:
                media.description ?: getString(R.string.not_available)
        tvArtist.text = media.artistName ?: getString(R.string.not_available)
        tvType.text = media.wrapperType ?: getString(R.string.not_available)
        tvCollection.text = media.collectionName ?: getString(R.string.not_available)
        tvReleaseDate.text = if(media.releaseDate == null) getString(R.string.not_available)
                                else Util.dateConverter(media.releaseDate!!)
        tvLength.text = Util.trackTimeConverter(media.trackTimeMillis)
        tvDisc.text = if(media.discNumber == 0) getString(R.string.not_available)
                        else getString(R.string.format_track, media.discNumber, media.discCount)
        tvTrack.text = if(media.trackNumber == 0) getString(R.string.not_available)
                        else getString(R.string.format_track, media.trackNumber, media.trackCount)
    }

    companion object {

        fun newInstance(): MediaDetailFragment {
            return MediaDetailFragment()
        }
    }
}
