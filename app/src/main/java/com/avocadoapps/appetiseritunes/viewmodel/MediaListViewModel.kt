package com.avocadoapps.appetiseritunes.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.avocadoapps.appetiseritunes.data.MediaRepository
import com.avocadoapps.appetiseritunes.data.remote.model.Media
import com.avocadoapps.appetiseritunes.data.remote.model.MediaListState

class MediaListViewModel (
    private val mediaRepository: MediaRepository
): ViewModel() {

    private val _medialiststate = MutableLiveData<MediaListState>()
    private val focusedMedia = MutableLiveData<Media>()

    fun getMediaListState(): LiveData<MediaListState> = _medialiststate
    fun getFocusedMedia(): LiveData<Media> = focusedMedia

    fun setFocusedMedia(media: Media) {
        this.focusedMedia.value = media
    }

    fun getTrackUrl(): String? {
        return this.focusedMedia.value?.trackViewUrl
    }

    fun fetchMedia() {
        _medialiststate.value = MediaListState.Loading
        mediaRepository.fetchMedia(_medialiststate)
    }
}