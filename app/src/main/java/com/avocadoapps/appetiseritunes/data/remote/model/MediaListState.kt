package com.avocadoapps.appetiseritunes.data.remote.model

sealed class MediaListState {
    object Loading: MediaListState()
    data class Success(val mediaList: ArrayList<Media>): MediaListState()
    data class Error(val error: String?): MediaListState()
}