## Notes

Master-detail mini app that shows a list of Media from this api: (as specified on requirements)
https://itunes.apple.com/search?term=star&amp;country=au&amp;media=movie&amp;all

I'm not sure if the API was supposed to be media=movie, or media=all. 
I used media=all for this project.

As per android guidelines, master-detail flow will be visible on wide screen devices like tablets.
Specifically width >w600dp devices. On phones, only the master list will be shown, clicking on a list item will shown more details.

I wasn't given a specific timeline on the deadline, so I decided to do a timebox of roughly 1.5 days to accomplish as much.
I decided to focus on the engineering rather than the UI given the time available, so the UI is pretty basic.

Since the fetch api is getting a variety of media types, some media data do not have certain attributes which will show
a placeholder text of "(Not available)". I noticed most of the K-pop tracks don't come with descriptions.

---

## Persistence

I decided to implement data persistence by using Room.

Whenever you fetch the media list from the api, it will also save a copy in the database.
This way, when you don't have internet connection, you can still see the most recent fetched media set.

---

## Architecture

This project is implemented using the MVVM architecture.

MVVM has been a widely adopted architecture in recent android development, with google aggressively promoting it with
the release of Android Architecture components.

Using MVVM promotes Single Responsibility with the view handling the interface and user interaction, 
repository (model) handling the data, and viewmodel stitching the two together. Making it easier to adjust one module
while minimally affecting the others.

Patterns:
1. Observer (RxJava/RxKotlin and architecture component's livedata)
2. Singleton (Repository, database, and api service)
3. Repository (Model in MVVM uses repository pattern)

