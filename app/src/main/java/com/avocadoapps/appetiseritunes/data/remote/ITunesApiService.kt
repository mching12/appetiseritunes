package com.avocadoapps.appetiseritunes.data.remote

import com.avocadoapps.appetiseritunes.data.remote.model.MediaResult
import io.reactivex.Observable
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Query

interface ITunesApiService {

//    https://itunes.apple.com/search?term=star&amp;country=au&amp;media=movie&amp;all

    @Headers("Content-Type: application/json")
    @GET("/search")
    fun searchMedia(
        @Query("term") query: String,
        @Query("country") country: String,
        @Query("media") media: String
    ): Observable<Response<MediaResult>>
}