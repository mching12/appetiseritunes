package com.avocadoapps.appetiseritunes.data.local

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters

@Database(entities = [MediaData::class], version = 1, exportSchema = false)
@TypeConverters
abstract class MediaDatabase : RoomDatabase() {

    abstract fun mediaDao(): MediaDao

    companion object {

        const val DATABASE_NAME = "media_database.db"

        @Volatile private var instance: MediaDatabase? = null
        private val LOCK = Any()

        operator fun invoke(context: Context)= instance ?: synchronized(LOCK){
            instance ?: buildDatabase(context).also { instance = it}
        }

        private fun buildDatabase(context: Context) =
            Room.databaseBuilder(context, MediaDatabase::class.java, DATABASE_NAME)
                .allowMainThreadQueries()
                .build()
    }
}