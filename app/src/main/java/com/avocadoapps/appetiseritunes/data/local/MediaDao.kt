package com.avocadoapps.appetiseritunes.data.local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Transaction

@Dao
abstract class MediaDao {

    @Insert
    abstract fun insertMedialist(forecastsDbList: List<MediaData>)

    @Query("SELECT * FROM media_data")
    abstract fun fetchMedialist(): List<MediaData>

    @Query("DELETE FROM media_data")
    abstract fun deleteAllMedia()

    @Transaction
    open fun refreshfreshData(mediaDbList: ArrayList<MediaData>) {
        deleteAllMedia()
        insertMedialist(mediaDbList)
    }
}
