@file:Suppress("DEPRECATION")

package com.avocadoapps.appetiseritunes.data.helper

import android.content.Context
import com.avocadoapps.appetiseritunes.data.local.MediaData
import com.avocadoapps.appetiseritunes.data.remote.model.Media
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.collections.ArrayList
import android.content.Context.CONNECTIVITY_SERVICE
import android.net.ConnectivityManager


object Util {

    fun dateConverter(string: String): String {
        val formatter =
            DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.ENGLISH)
        return LocalDate.parse(string, formatter).toString()
    }

    fun trackTimeConverter(millis: Long): String {
        return String.format(
            "%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(millis),
            TimeUnit.MILLISECONDS.toMinutes(millis) % TimeUnit.HOURS.toMinutes(1),
            TimeUnit.MILLISECONDS.toSeconds(millis) % TimeUnit.MINUTES.toSeconds(1)
        )
    }

    fun isInternetAvailable(context: Context): Boolean {
        val cm = context.getSystemService(CONNECTIVITY_SERVICE) as ConnectivityManager
        return try {
            val activeNetwork = cm.activeNetworkInfo
            activeNetwork != null && activeNetwork.isConnectedOrConnecting
        } catch (e: Exception) {
            e.printStackTrace()
            false
        }
    }

    fun roomToRetrofit(mediaDataList: ArrayList<MediaData>): ArrayList<Media> {
        val medialist = arrayListOf<Media>()
        mediaDataList.forEach {
            val media = Media(it.wrapperType, it.kind, it.artistId, it.collectionId,
                it.trackId, it.artistName, it.collectionName, it.trackName, it.collectionCensoredName,
                it.trackCensoredName, it.artistViewUrl, it.collectionViewUrl, it.trackViewUrl,
                it.previewUrl, it.artworkUrl30, it.artworkUrl60, it.artworkUrl100, it.collectionPrice,
                it.trackPrice, it.releaseDate, it.collectionExplicitness, it.trackExplicitness,
                it.discCount, it.discNumber, it.trackCount, it.trackNumber, it.trackTimeMillis,
                it.country, it.currency, it.primaryGenreName, it.contentAdvisoryRating,
                it.shortDescription, it.longDescription, it.description, it.isStreamable)
            medialist.add(media)
        }
        return medialist
    }

    fun retrofitToRoom(medialist: ArrayList<Media>): ArrayList<MediaData> {
        val mediadatalist = arrayListOf<MediaData>()
        medialist.forEach {
            val mediadata = MediaData(it.trackId, it.wrapperType, it.kind, it.artistId,
                it.collectionId, it.artistName, it.collectionName, it.trackName,
                it.collectionCensoredName, it.trackCensoredName, it.artistViewUrl, it.collectionViewUrl,
                it.trackViewUrl, it.previewUrl, it.artworkUrl30, it.artworkUrl60, it.artworkUrl100,
                it.collectionPrice, it.trackPrice, it.releaseDate, it.collectionExplicitness,
                it.trackExplicitness, it.discCount, it.discNumber, it.trackCount, it.trackNumber,
                it.trackTimeMillis, it.country, it.currency, it.primaryGenreName, it.contentAdvisoryRating,
                it.longDescription, it.shortDescription, it.description, it.isStreamable)
            mediadatalist.add(mediadata)
        }
        return mediadatalist
    }
}