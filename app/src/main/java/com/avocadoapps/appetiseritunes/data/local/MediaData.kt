package com.avocadoapps.appetiseritunes.data.local

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity(tableName = "media_data")
data class MediaData(
    @PrimaryKey
    var trackId: Double,
    @ColumnInfo(name = "wrapper_type")
    var wrapperType: String? = null,
    @ColumnInfo(name = "kind")
    var kind: String? = null,
    @ColumnInfo(name = "artist_id")
    var artistId: Double = 0.toDouble(),
    @ColumnInfo(name = "collection_id")
    var collectionId: Double = 0.toDouble(),
    @ColumnInfo(name = "artist_name")
    var artistName: String? = null,
    @ColumnInfo(name = "collection_name")
    var collectionName: String? = null,
    @ColumnInfo(name = "track_name")
    var trackName: String? = null,
    @ColumnInfo(name = "collection_censored_name")
    var collectionCensoredName: String? = null,
    @ColumnInfo(name = "track_censored_name")
    var trackCensoredName: String? = null,
    @ColumnInfo(name = "artist_view_url")
    var artistViewUrl: String? = null,
    @ColumnInfo(name = "collection_view_url")
    var collectionViewUrl: String? = null,
    @ColumnInfo(name = "track_view_url")
    var trackViewUrl: String? = null,
    @ColumnInfo(name = "preview_url")
    var previewUrl: String? = null,
    @ColumnInfo(name = "artwork_url_30")
    var artworkUrl30: String? = null,
    @ColumnInfo(name = "artwork_url_60")
    var artworkUrl60: String? = null,
    @ColumnInfo(name = "artwork_url_100")
    var artworkUrl100: String? = null,
    @ColumnInfo(name = "collection_price")
    var collectionPrice: Float = 0.toFloat(),
    @ColumnInfo(name = "track_price")
    var trackPrice: Float = 0.toFloat(),
    @ColumnInfo(name = "release_date")
    var releaseDate: String? = null,
    @ColumnInfo(name = "collection_explicitness")
    var collectionExplicitness: String? = null,
    @ColumnInfo(name = "track_explicitness")
    var trackExplicitness: String? = null,
    @ColumnInfo(name = "disc_count")
    var discCount: Int = 0,
    @ColumnInfo(name = "disc_number")
    var discNumber: Int = 0,
    @ColumnInfo(name = "track_count")
    var trackCount: Int = 0,
    @ColumnInfo(name = "track_number")
    var trackNumber: Int = 0,
    @ColumnInfo(name = "track_time_millis")
    var trackTimeMillis: Long = 0.toLong(),
    @ColumnInfo(name = "country")
    var country: String? = null,
    @ColumnInfo(name = "currency")
    var currency: String? = null,
    @ColumnInfo(name = "primary_genre_name")
    var primaryGenreName: String? = null,
    @ColumnInfo(name = "content_advisory_rating")
    var contentAdvisoryRating: String? = null,
    @ColumnInfo(name = "long_description")
    var longDescription: String? = null,
    @ColumnInfo(name = "short_description")
    var shortDescription: String? = null,
    @ColumnInfo(name = "description")
    var description: String? = null,
    @ColumnInfo(name = "is_streamable")
    var isStreamable: Boolean = false
)