package com.avocadoapps.appetiseritunes.view

import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.avocadoapps.appetiseritunes.R
import com.avocadoapps.appetiseritunes.data.remote.model.Media
import com.avocadoapps.appetiseritunes.data.remote.model.MediaListState

import com.avocadoapps.appetiseritunes.view.adapter.MediaAdapter
import com.avocadoapps.appetiseritunes.view.fragment.MediaDetailFragment
import com.avocadoapps.appetiseritunes.view.listener.OnMediaSelectListener
import com.avocadoapps.appetiseritunes.viewmodel.MediaListViewModel
import kotlinx.android.synthetic.main.activity_item_list.*
import kotlinx.android.synthetic.main.item_list.*
import org.koin.androidx.viewmodel.ext.android.viewModel

/**
 * An activity representing a list of Pings. This activity
 * has different presentations for handset and tablet-size devices. On
 * handsets, the activity presents a list of items, which when touched,
 * lead to a [MediaDetailActivity] representing
 * item details. On tablets, the activity presents the list of items and
 * item details side-by-side using two vertical panes.
 */
class MediaListActivity : BaseActivity(), OnMediaSelectListener,
    SwipeRefreshLayout.OnRefreshListener {

    /**
     * Whether or not the activity is in two-pane mode, i.e. running on a tablet
     * device.
     */
    private var twoPane: Boolean = false
    private val viewModel: MediaListViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_item_list)

        initUi()
        initViewModel()
        loadMedia(arrayListOf())
    }

    override fun onMediaSelected(media: Media) {
        if (twoPane) viewModel.setFocusedMedia(media)
        else MediaDetailActivity.launch(this, media)
    }

    override fun onRefresh() {
        viewModel.fetchMedia()
    }

    private fun initUi() {
        setSupportActionBar(toolbar)
        toolbar.title = title
        refreshLayout.setOnRefreshListener(this)
        if (item_detail_container != null) {
            twoPane = true
            showMediaDetailsFragment()
        }
    }

    private fun initViewModel() {
        viewModel.getMediaListState().observe(this, Observer {
            renderMediaListState(it)
        })
        viewModel.fetchMedia()
    }

    private fun loadMedia(mediaList: ArrayList<Media>) {
        item_list.apply {
            adapter = MediaAdapter(this@MediaListActivity, mediaList)
            addItemDecoration(DividerItemDecoration(this@MediaListActivity,
                LinearLayoutManager.VERTICAL))
        }
    }

    private fun renderMediaListState(mediaListState: MediaListState) {
        refreshLayout.isRefreshing = mediaListState is MediaListState.Loading
        when(mediaListState) {
            is MediaListState.Success -> loadMedia(mediaListState.mediaList)
            is MediaListState.Error -> showSnackbar(parentLayout,
                mediaListState.error ?: getString(R.string.error_getting_media))
        }
    }

    private fun showMediaDetailsFragment() {
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.item_detail_container,
                MediaDetailFragment.newInstance())
            .commit()
    }
}
